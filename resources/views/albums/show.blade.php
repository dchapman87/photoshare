@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <button class="btn btn-primary ml-auto" onclick="window.location.href = window.location.href + '/create'">Add Photo</button>
    </div>
    <div class="row">
        <div class="col-3 pt-1 pl-5 pr-3">
            <img class="rounded-circle" src="/storage/{{ $album->logo }}" alt="Album title">
        </div>
        <div class="col-9">
            <div class="pt-1">
                <h1>{{ $album->title }}</h1>
            </div>
            <div class="d-flex">
                <div class="pr-3"><strong>100</strong> photos</div>
                <div class="pr-3"><strong>12</strong> followers</div>
            </div>
            <div class="pt-4">
                <p>{{ $album->description }}</p>
            </div>
        </div>
    </div>
    <photo-grid url="{{ $album->photos_url }}"/>
</div>
@endsection
