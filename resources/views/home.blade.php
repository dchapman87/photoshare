@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h2 class="pt-2">Albums</h2>
        <button onclick="window.location.href='/albums/create'" class="btn btn-primary ml-auto">New Album</button>
    </div>
    <album-grid userid="{{ Auth::user()->id }}">
</div>
@endsection
