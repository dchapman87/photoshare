import Vue from 'vue'
import { HasError, AlertError, AlertSuccess } from 'vform'
import NewAlbumForm from './NewAlbumForm'
import PhotoGrid from './PhotoGrid'
import AlbumGrid from './AlbumGrid'
import VoerroTagsInput from '@voerro/vue-tagsinput';


// Components that are registered globaly.
[
  HasError,
  AlertError,
  AlertSuccess,
  NewAlbumForm,
  AlbumGrid,
  PhotoGrid
].forEach(Component => {
  Vue.component(Component.name, Component)
})

Vue.component('tags-input', VoerroTagsInput)
import '@voerro/vue-tagsinput/dist/style.css'
