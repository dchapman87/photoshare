# PhotoShare (Not Completed)

A simple photo sharing app to demo using various parts of the laravel stack

## Stack Used

* Laravel 5.8
    * Passport
    * Horizon
    * Telescope
* Bootstrap
* Vue.JS
* Mongodb
* Minio (or S3)
* Redis
* Docker

## Design Brief

Photo Share should: 

* Be easily deployable and preconfigured via docker for the whole stack (see `./docker` directory)
* Be a very simple app that you can upload images into albums for viewing & sharing
* Use Mongodb for the database
* Provide a REST API using laravel-passport - that both users and the frontend can use (see `app/Http/Controllers/Api` directory)
* Frontend implemented in a mixture of Blade and Vue.JS components
* Horizon should be used to perform image resizing in the background
* The API should be tested using phpunit for all endpoints ( see `tests/Feature/Api` directory)

## Getting Started

The dependencies required on the host machine are only:

* [docker](https://docker.com)
* [docker-compose](https://docs.docker.com/compose/)

Everything needed for development is within the workspace container

## Running with Docker

The first run is going to take a while as it needs to build all the containers, but subsequent runs will be much quicker.

From the root of the repo run

```sh
$ ./run-local
```

This will go through the process of:

* Building the containers
* Starting them up
* Install php and js dependencies
* compile js assets
* Configure passport
* Run unit-tests to ensure it's all up and working

To stop the services you can use the `./cleanup-local` script in the root of the repo

## Composer

Composer can be used via the `./composer` script in the root of the repo

```sh
$ ./composer require ...
```

## Yarn

Yarn can be used via the './yarn' script in the root of the repo

```sh
$ ./yarn run dev
```

## Running Tests Manually

To run tests manually use the `./run-tests` script in root of repo





