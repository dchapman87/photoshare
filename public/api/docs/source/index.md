---
title: API Reference

language_tabs:
- bash
- javascript
- php

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/api/docs/collection.json)

<!-- END_INFO -->

#general
<!-- START_817abd68e16819efff9a5c4e3e957ce4 -->
## Display a listing of albums for given user.

> Example request:

```bash
curl -X GET -G "/api/v1/users/1/albums" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL("/api/v1/users/1/albums");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get("/api/v1/users/1/albums", [
    'headers' => [
            "Authorization" => "Bearer {token}",
        ],
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



> Example response:

```json
null
```

### HTTP Request
`GET api/v1/users/{user}/albums`


<!-- END_817abd68e16819efff9a5c4e3e957ce4 -->

<!-- START_b93797c43db7de5cceca6ea7faebcb8c -->
## Store a new album

> Example request:

```bash
curl -X POST "/api/v1/users/1/albums" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL("/api/v1/users/1/albums");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post("/api/v1/users/1/albums", [
    'headers' => [
            "Authorization" => "Bearer {token}",
        ],
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```




### HTTP Request
`POST api/v1/users/{user}/albums`


<!-- END_b93797c43db7de5cceca6ea7faebcb8c -->

<!-- START_d3a0e8d06e736c78268f41933390a643 -->
## Display a users album

> Example request:

```bash
curl -X GET -G "/api/v1/users/1/albums/1" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL("/api/v1/users/1/albums/1");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get("/api/v1/users/1/albums/1", [
    'headers' => [
            "Authorization" => "Bearer {token}",
        ],
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



> Example response:

```json
null
```

### HTTP Request
`GET api/v1/users/{user}/albums/{album}`


<!-- END_d3a0e8d06e736c78268f41933390a643 -->

<!-- START_8a2221ac5eb42cc98b4404582aacc76c -->
## Update a users album.

> Example request:

```bash
curl -X PUT "/api/v1/users/1/albums/1" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL("/api/v1/users/1/albums/1");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put("/api/v1/users/1/albums/1", [
    'headers' => [
            "Authorization" => "Bearer {token}",
        ],
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```




### HTTP Request
`PUT api/v1/users/{user}/albums/{album}`

`PATCH api/v1/users/{user}/albums/{album}`


<!-- END_8a2221ac5eb42cc98b4404582aacc76c -->

<!-- START_1190050cc2d161381499f1f05d7a8e86 -->
## Remove a users album.

> Example request:

```bash
curl -X DELETE "/api/v1/users/1/albums/1" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL("/api/v1/users/1/albums/1");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete("/api/v1/users/1/albums/1", [
    'headers' => [
            "Authorization" => "Bearer {token}",
        ],
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```




### HTTP Request
`DELETE api/v1/users/{user}/albums/{album}`


<!-- END_1190050cc2d161381499f1f05d7a8e86 -->

<!-- START_4726c51b2d9d668a9bc25b4af11da4e0 -->
## Display albums.

> Example request:

```bash
curl -X GET -G "/api/v1/albums" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL("/api/v1/albums");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get("/api/v1/albums", [
    'headers' => [
            "Authorization" => "Bearer {token}",
        ],
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



> Example response:

```json
null
```

### HTTP Request
`GET api/v1/albums`


<!-- END_4726c51b2d9d668a9bc25b4af11da4e0 -->

<!-- START_e4d980d2136a48c85c2800f3a1a509e2 -->
## Store a new album.

> Example request:

```bash
curl -X POST "/api/v1/albums" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL("/api/v1/albums");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post("/api/v1/albums", [
    'headers' => [
            "Authorization" => "Bearer {token}",
        ],
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```




### HTTP Request
`POST api/v1/albums`


<!-- END_e4d980d2136a48c85c2800f3a1a509e2 -->

<!-- START_cd7232bcb07171ece3b38e6114515529 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET -G "/api/v1/albums/1" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL("/api/v1/albums/1");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get("/api/v1/albums/1", [
    'headers' => [
            "Authorization" => "Bearer {token}",
        ],
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



> Example response:

```json
null
```

### HTTP Request
`GET api/v1/albums/{album}`


<!-- END_cd7232bcb07171ece3b38e6114515529 -->

<!-- START_842d9981f7b5a6207577907853d4ba26 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "/api/v1/albums/1" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL("/api/v1/albums/1");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put("/api/v1/albums/1", [
    'headers' => [
            "Authorization" => "Bearer {token}",
        ],
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```




### HTTP Request
`PUT api/v1/albums/{album}`

`PATCH api/v1/albums/{album}`


<!-- END_842d9981f7b5a6207577907853d4ba26 -->

<!-- START_1d9ff707089250799f38d3320e40efe4 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "/api/v1/albums/1" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL("/api/v1/albums/1");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete("/api/v1/albums/1", [
    'headers' => [
            "Authorization" => "Bearer {token}",
        ],
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```




### HTTP Request
`DELETE api/v1/albums/{album}`


<!-- END_1d9ff707089250799f38d3320e40efe4 -->

<!-- START_625b5a9f61d90a935e709502cb515b53 -->
## Display a list of album photos.

> Example request:

```bash
curl -X GET -G "/api/v1/albums/1/photos" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL("/api/v1/albums/1/photos");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get("/api/v1/albums/1/photos", [
    'headers' => [
            "Authorization" => "Bearer {token}",
        ],
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



> Example response:

```json
null
```

### HTTP Request
`GET api/v1/albums/{album}/photos`


<!-- END_625b5a9f61d90a935e709502cb515b53 -->

<!-- START_7f6cb2ffb2880d9b37e46df8614c96d3 -->
## Store a newly created photo.

> Example request:

```bash
curl -X POST "/api/v1/albums/1/photos" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL("/api/v1/albums/1/photos");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post("/api/v1/albums/1/photos", [
    'headers' => [
            "Authorization" => "Bearer {token}",
        ],
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```




### HTTP Request
`POST api/v1/albums/{album}/photos`


<!-- END_7f6cb2ffb2880d9b37e46df8614c96d3 -->

<!-- START_8ed3048eb703169461f23e3836812931 -->
## Display a photo.

> Example request:

```bash
curl -X GET -G "/api/v1/albums/1/photos/1" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL("/api/v1/albums/1/photos/1");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get("/api/v1/albums/1/photos/1", [
    'headers' => [
            "Authorization" => "Bearer {token}",
        ],
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



> Example response:

```json
null
```

### HTTP Request
`GET api/v1/albums/{album}/photos/{photo}`


<!-- END_8ed3048eb703169461f23e3836812931 -->

<!-- START_fd51ca79d72e46202d3911194742d4b7 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "/api/v1/albums/1/photos/1" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL("/api/v1/albums/1/photos/1");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put("/api/v1/albums/1/photos/1", [
    'headers' => [
            "Authorization" => "Bearer {token}",
        ],
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```




### HTTP Request
`PUT api/v1/albums/{album}/photos/{photo}`

`PATCH api/v1/albums/{album}/photos/{photo}`


<!-- END_fd51ca79d72e46202d3911194742d4b7 -->

<!-- START_93533cb5b3e77c14f4568b6d90fd2709 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "/api/v1/albums/1/photos/1" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL("/api/v1/albums/1/photos/1");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete("/api/v1/albums/1/photos/1", [
    'headers' => [
            "Authorization" => "Bearer {token}",
        ],
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```




### HTTP Request
`DELETE api/v1/albums/{album}/photos/{photo}`


<!-- END_93533cb5b3e77c14f4568b6d90fd2709 -->

<!-- START_16fd38b60f1ec6e16e4811bc19196f9c -->
## Display uploaded photos.

> Example request:

```bash
curl -X GET -G "/api/v1/photos" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL("/api/v1/photos");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get("/api/v1/photos", [
    'headers' => [
            "Authorization" => "Bearer {token}",
        ],
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



> Example response:

```json
null
```

### HTTP Request
`GET api/v1/photos`


<!-- END_16fd38b60f1ec6e16e4811bc19196f9c -->


