<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;
use App\Photo;

$factory->define(Photo::class, function (Faker $faker) {
    return [
        'caption' => $faker->sentence,
        'image' => $faker->image,
        'tags' => ['ptag1', 'ptag2']
    ];
});
