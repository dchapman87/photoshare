<?php

use Illuminate\Database\Seeder;
use App\Album;
use App\Photo;

class PhotosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(Album::all() as $album) {
            $photo = factory(Photo::class)->create();
            $album->photos()->save($photo);
            $album->user->photos()->save($photo);
        }
    }
}
