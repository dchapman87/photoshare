<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Album;

class AlbumsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (User::all() as $user) {
            $album = factory(Album::class)->create();
            $user->albums()->save($album);
        }
    }
}
