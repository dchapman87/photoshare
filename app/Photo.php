<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;
use App\Traits\PhotosFilterPaginate;
use URL;

/**
 * @OA\Schema(
 *      required={"caption", "image"},
 *      @OA\Property(property="caption", type="string"),
 *      @OA\Property(property="image", type="string")
 * )
 */
class Photo extends Model
{
    /**
     *
     */

    use PhotosFilterPaginate;

    protected $fillable = [
        'caption',
        'image',
        'comments',
        'tags'
    ];

    protected $appends = [
        'comments_url',
        'image_url'
    ];

    public function album()
    {
        return $this->belongsTo(Album::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function getImageUrlAttribute()
    {
        return URL::to(sprintf('storage/%s', $this->image));
    }

    public function getCommentsUrlAttribute()
    {
        return URL::to(
            sprintf(
                'api/v1/albums/%s/photos/%s/comments',
                $this->album->id,
                $this->id
            )
        );
    }
}
