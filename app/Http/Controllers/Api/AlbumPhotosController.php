<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Album;
use App\Photo;
use App\Events\PhotoCreated;
use App\Events\PhotoUpdated;
use App\Events\PhotoDeleted;

class AlbumPhotosController extends Controller
{
    /**
     * Display a list of album photos.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Album $album)
    {
        return Photo::where('album_id', $album->id)->filterPaginateOrder();
    }

    /**
     * Store a newly created photo.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Album $album)
    {
        $attributes = $request->validate([
            'caption' => ['required', 'max:120', 'string'],
            'image' => ['required', 'file'],
            'tags' => ['array'],
        ]);
        if (!is_null($request->image)) {
            $attributes['image'] = $request->file('image')->store('photos');
        }
        $photo = $this->savePhoto($album, Photo::create($attributes));
        event(new PhotoCreated($photo));
        return response()->json($photo, 201);
    }

    /**
     * Display a photo.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Album $album, Photo $photo)
    {
        return response()->json($photo, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Album $album, Photo $photo)
    {
        $attributes = $request->validate([
            'caption' => ['max:120', 'string'],
            'tags' => ['array'],
        ]);
        $photo->update($attributes);
        $photo->save();
        event(new PhotoUpdated($photo));
        return response()->json($photo, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Album $album, Photo $photo)
    {
        if (!$photo->delete()) {
            return response()->json(['message' => 'Failed deleting photo'], 500);
        }
        event(new PhotoDeleted($photo));
        return response()->json(['message' => 'Photo deleted'], 204);
    }

    private function savePhoto(Album $album, Photo $photo)
    {
        $album->photos()->save($photo);
        $user = request()->user();
        return $user->photos()->save($photo);
    }
}
