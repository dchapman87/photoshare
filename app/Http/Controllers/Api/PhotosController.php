<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Photo;

class PhotosController extends Controller
{
    /**
     * Display uploaded photos.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Photo::with('album')->filterPaginateOrder();
    }

    /**
     * Download a photo via it's id
     */
    public function show(Request $request, Photo $photo)
    {
        return Storage::download($photo->image);
    }
}
