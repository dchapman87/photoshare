<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use App\User;
use App\Album;

class UserAlbumsController extends Controller
{
    /**
     * Display a listing of albums for given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, User $user)
    {
        return Album::where('user_id', $user->id)->filterPaginateOrder();
    }

    /**
     * Store a new album
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $req = Request::create('api/v1/albums', 'POST', $request->toArray());
        return Route::dispatch($req);
    }

    /**
     * Display a users album
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User $user
     * @param  \App\Album $album
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $user, Album $album)
    {
        return response()->json($album, 200);
    }

    /**
     * Update a users album.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User $user
     * @param  \App\Album $album
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user, Album $album)
    {
        $req = Request::create(
            sprintf('api/v1/albums/%s', $album->id),
            'PATCH',
            $request->toArray()
        );
        return Route::dispatch($req);
    }

    /**
     * Remove a users album.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User $user
     * @param  \App\Album $album
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $user, Album $album)
    {
        $req = Request::create(
            sprintf('api/v1/albums/%s', $album->id),
            'DELETE',
            $request->toArray()
        );
        return Route::dispatch($req);
    }
}
