<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Album;
use App\Rules\TitleUniqueForUser;
use App\Events\AlbumCreated;
use App\Events\AlbumUpdated;
use App\Events\AlbumDeleted;
use function GuzzleHttp\json_decode;

class AlbumsController extends Controller
{
    /**
     * Display albums.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Album::filterPaginateOrder();
    }

    /**
     * Store a new album.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $this->validateAttributes($request);
        $album = $this->saveAlbum(Album::create($attributes));
        event(new AlbumCreated($album));
        return response()->json($album, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Album $album)
    {
        return response()->json($album, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Album $album)
    {
        $attributes = $this->validateAttributes($request);
        $album->update($attributes);
        $album->save();
        event(new AlbumUpdated($album));
        return response()->json($album, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Album $album)
    {
        if (!$album->delete()) {
            return response()->json(['message' => 'Failed deleting album'], 500);
        }
        event(new AlbumDeleted($album));
        return response()->json(['message' => 'Album deleted'], 204);
    }

    private function saveAlbum(Album $album)
    {
        $user = request()->user();
        return $user->albums()->save($album);
    }

    private function validateAttributes(Request $request)
    {
        $attributes = $request->validate($this->rules());
        if (!is_null($request->logo)) {
            $attributes['logo'] = $request->file('logo')->store('logos');
            // dd($attributes);
        }
        if (!is_null($request->tags)) {
            $attributes['tags'] = json_decode($request->tags, true);
        }
        return $attributes;
    }

    private function rules() {
        return [
            'title' => ['required', 'max:30', 'string', new TitleUniqueForUser],
            'description' => ['string', 'max:240'],
            'logo' => ['file'],
            'tags' => ['string'],
        ];
    }
}
