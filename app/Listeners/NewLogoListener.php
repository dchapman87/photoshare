<?php

namespace App\Listeners;

use App\Events\AlbumCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Image;

class NewLogoListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AlbumCreated  $event
     * @return void
     */
    public function handle(AlbumCreated $event)
    {
        $album = $event->album;
        $img = Image::make(public_path('storage/'.$album->logo))->resize(100, 100);
        $img->pixelate();
        $img->greyscale();
        $img->save(public_path('storage/'.$album->logo));
    }
}
