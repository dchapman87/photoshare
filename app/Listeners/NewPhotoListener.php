<?php

namespace App\Listeners;

use App\Events\PhotoCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewPhotoListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PhotoCreated  $event
     * @return void
     */
    public function handle(PhotoCreated $event)
    {
        // TODO: Resize image
    }
}
