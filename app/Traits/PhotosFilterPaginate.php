<?php
namespace App\Traits;

trait PhotosFilterPaginate {
    
    public function scopeFilterPaginateOrder($query)
    {
        $request = request();

        $query = $query->orderBy($request->column ?: 'title', $request->direction ?: 'asc');
        if (!is_null($request->search_query)) {
            foreach(json_decode($request->search_query) as $key => $value) {
                if (empty($value)) {
                    continue;
                }
                switch ($key) {
                    # Tags takes an array of tags to query
                    case 'tags':
                        $query = $query->where($key, 'all', $value);
                        break;
                    default:
                        # Compare any other keys with a where like query
                        $query = $query->where($key, 'like', '%'.$value.'%');
                        break;
                }
            }
        }

        return $query->paginate((int)$request->per_page ?: 15);
    }
}