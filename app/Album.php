<?php

namespace App;

use App\User;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Traits\AlbumsFilterPaginate;
use URL;

class Album extends Eloquent
{
    use AlbumsFilterPaginate;

    protected $fillable = [
        'title',
        'description',
        'logo',
        'tags',
        'followerIds'
    ];

    protected $appends = [
        'photos_url',
        'logo_url'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    public function getPhotosUrlAttribute()
    {
        return URL::to(
            sprintf('api/v1/albums/%s/photos', $this->id)
        );
    }

    public function getLogoUrlAttribute()
    {
        return URL::to(sprintf('storage/%s', $this->logo));
    }

}
