<?php

namespace Tests;

use Tests\TestCase;
use League\JsonReference\Dereferencer;
use League\JsonGuard\Validator as JsonValidator;
use Laravel\Passport\Passport;
use App\User;
use Illuminate\Support\Facades\Event;
use DB;

abstract class ApiTestCase extends TestCase
{

    protected $user = null;

    public function setUp() : void
    {
        parent::setUp();
        $this->seed(\UsersSeeder::class);
        $this->seed(\AlbumsSeeder::class);
        $this->user = User::first();
        Passport::actingAs($this->user, ['*']);
        Event::fake();
    }

    # This is ugly has hell but it's the only
    # way I could figure out how to cleanup the collections
    # after the test run. As moloquent doesn't support dropIfExists
    # which the laravel-passport migrations use and make it puke.
    # Meaning we can't use the DatabaseMigrations or RefreshDatabase traits :-(
    public function tearDown() : void
    {
        DB::collection('users')->delete();
        DB::collection('albums')->delete();
        DB::collection('photos')->delete();
        DB::collection('comments')->delete();
    }

    private $schemaDir = 'file://' . __DIR__ . '/../schemas/';

    protected function loadSchema($schema)
    {
        $dereferencer = Dereferencer::draft4();
        $schema = $dereferencer->dereference($this->schemaDir . $schema);
        return $schema;
    }

    protected function assertJsonSchema($data, $schema)
    {
        $validator = new JsonValidator($data, $this->loadSchema($schema));
        if ($validator->fails()) {
            dd($validator->errors());
        }
        $this->assertTrue($validator->passes());
    }
}