<?php

namespace Tests\Feature;

use Tests\ApiTestCase;
use App\Photo;

class PhotosTest extends ApiTestCase
{
    /**
     * Test the route exists.
     *
     * @return void
     */
    public function testRouteExists()
    {
        $response = $this->get('api/v1/photos');
        $response->assertStatus(200);
    }

    /**
     * Test the json response structure using json schema.
     */
    public function test_json_structure_for_paginated_response()
    {
        $this->seed(\PhotosSeeder::class);
        $response = $this->get(sprintf('api/v1/photos'));
        $response->assertStatus(200);
        $this->assertJsonSchema(json_decode($response->content()), 'Photos.json');
    }

}