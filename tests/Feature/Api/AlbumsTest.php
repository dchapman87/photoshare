<?php

namespace Tests\Feature;

use Tests\ApiTestCase;
use Illuminate\Support\Facades\Event;
use App\Events\AlbumCreated;
use App\Events\AlbumUpdated;
use App\Events\AlbumDeleted;
use App\Album;

class AlbumsTest extends ApiTestCase
{
    /**
     * Test the route exists.
     *
     * @return void
     */
    public function testRouteExists()
    {
        $response = $this->get('api/v1/albums');
        $response->assertStatus(200);
    }

    /**
     * Test the json response structure using json schema.
     */
    public function test_json_structure_for_paginated_response()
    {
        $response = $this->get('/api/v1/albums');
        $response->assertStatus(200);
        $this->assertJsonSchema(json_decode($response->content()), 'Albums.json');
    }

    /**
     * Test the default attributes.
     *
     * column=created_at
     * direction=desc
     * per_page=15
     *
     * @return void
     */
    public function test_api_returns_index_in_default_order()
    {
        $response = $this->get('/api/v1/albums');
        $response->assertStatus(200);
        $data = json_decode($response->content());
        $albums = collect($data->data);
        $this->assertTrue($albums->first()->created_at >= $albums->last()->created_at);
        $this->assertEquals($data->per_page, 15);
    }

    /**
     * Test the custom attributes.
     *
     * column=title
     * direction=desc
     * per_page=4
     *
     * @return void
     */
    public function test_api_returns_index_in_custom_order()
    {
        $response = $this->get('/api/v1/albums?column=title&direction=desc&per_page=4');
        $response->assertStatus(200);
        $data = json_decode($response->content());
        $albums = collect($data->data);
        $this->assertTrue($albums->first()->title >= $albums->last()->title);
        $this->assertEquals($data->per_page, 4);
        $this->assertEquals($albums->count(), 4);
    }

    /**
     * Test search by attribute
     *
     * Search for albums by user_id
     */
    public function test_api_search_by_user_id()
    {
        $response = $this->get(sprintf('/api/v1/albums?column=title&direction=desc&per_page=4&search_query={"user_id": "%s"}', $this->user->id));
        $response->assertStatus(200);
        $data = json_decode($response->content());
        $this->assertEquals($data->total, 1);
    }

    /**
     * Test creating a new album
     */
    public function test_can_create_new_album()
    {
        $album = [
            'title' => 'Test Album',
            'description' => 'Album for testing',
            'tags' => [
                'tag1',
                'tag2',
                'tag3'
            ]
        ];
        $before = $this->user->albums->count();
        $response = $this->postJson('api/v1/albums', $album);
        $response->assertStatus(201);
        $created = json_decode($response->content());
        $this->assertEquals($created->title, $album['title']);
        $this->assertEquals(3, collect($created->tags)->count());
        $after = $this->user->fresh()->albums->count();
        $this->assertTrue($before < $after);
        $this->assertEquals($before + 1, $after);
    }

    public function test_create_album_raises_create_event()
    {
        Event::fake();
        $album = [
            'title' => 'Test Album',
            'description' => 'Album for testing',
            'tags' => [
                'tag1',
                'tag2',
                'tag3'
            ]
        ];
        $response = $this->postJson('api/v1/albums', $album);
        $response->assertStatus(201);
        $created = json_decode($response->content(), true);
        Event::assertDispatched(AlbumCreated::class, function ($e) use ($created) {
            return $e->album->id === $created['_id'];
        });
    }

    public function test_create_album_with_invalid_fields()
    {
        $response = $this->postJson('api/v1/albums', []);
        $response->assertStatus(422);
        $data = json_decode($response->content(), true)['errors'];
        $this->assertArrayHasKey('title', $data);
    }

    public function test_can_update_album()
    {
        $album = Album::first();
        $album->title = "New Title";
        $response = $this->patchJson(
            sprintf('api/v1/albums/%s', $album->id),
            $album->toArray()
        );
        $response->assertStatus(200);
        $updated = json_decode($response->content());
        $this->assertEquals($updated->_id, $album->id);
        $this->assertEquals($updated->title, $album->title);
    }

    public function test_update_album_fires_updated_event()
    {
        Event::fake();
        $album = Album::first();
        $album->title = "New Title";
        $response = $this->patchJson(
            sprintf('api/v1/albums/%s', $album->id),
            $album->toArray()
        );
        $response->assertStatus(200);
        $updated = json_decode($response->content(), true);
        Event::assertDispatched(AlbumUpdated::class, function ($e) use ($updated) {
            return $e->album->id === $updated['_id'];
        });
    }

    public function test_delete_album()
    {
        $album = Album::first();
        $albumCount = Album::all()->count();
        $response = $this->delete(sprintf('api/v1/albums/%s', $album->id));
        $response->assertStatus(204);
        $this->assertEquals(Album::all()->count(), $albumCount - 1);
    }

    public function test_delete_album_fires_deleted_event()
    {
        Event::fake();
        $album = Album::first();
        $response = $this->delete(sprintf('api/v1/albums/%s', $album->id));
        $response->assertStatus(204);
        Event::assertDispatched(AlbumDeleted::class, 1);
    }

}
