<?php

namespace Tests\Feature;

use Tests\ApiTestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Event;
use App\Events\PhotoCreated;
use App\Events\PhotoUpdated;
use App\Events\PhotoDeleted;
use App\Album;
use App\Photo;

class AlbumPhotosTest extends ApiTestCase
{
    public function setUp() : void
    {
        parent::setUp();
    }
    /**
     * Test the route exists.
     *
     * @return void
     */
    public function testRouteExists()
    {
        $response = $this->get(sprintf('api/v1/albums/%s/photos', Album::first()->id));
        $response->assertStatus(200);
    }

    /**
     * Test the json response structure using json schema.
     */
    public function test_json_structure_for_paginated_response()
    {
        $this->seed(\PhotosSeeder::class);
        $album = Album::first();
        $response = $this->get(
            sprintf('api/v1/albums/%s/photos', $album->id)
        );
        $response->assertStatus(200);
        $this->assertJsonSchema(json_decode($response->content()), 'Photos.json');
    }

    public function test_can_create_new_photo()
    {
        Storage::fake('photos');
        $photo = [
            'caption' => "Test Photo Upload",
            'image' => UploadedFile::fake()->image('photo.jpg'),
            'tags' => [
                'tag1',
                'tag2',
                'tag3'
            ]
        ];
        $album = Album::first();
        $response = $this->postJson(
            sprintf('api/v1/albums/%s/photos', $album->id),
            $photo
        );
        $response->assertStatus(201);
        $created = json_decode($response->content());
        $this->assertEquals($created->caption, $photo['caption']);
        $this->assertEquals($created->album_id, $album->id);
        Storage::assertExists($created->image);
    }

    public function test_can_update_album_photo()
    {
        $this->seed(\PhotosSeeder::class);
        $photo = Photo::first();
        $photo->caption = "New Caption";
        $response = $this->patchJson(
            sprintf('api/v1/albums/%s/photos/%s', $photo->album_id, $photo->id),
            $photo->toArray()
        );
        // dd($response);
        $response->assertStatus(200);
        $updated = json_decode($response->content());
        $this->assertEquals($updated->_id, $photo->id);
        $this->assertEquals($updated->caption, $photo->caption);
    }
}