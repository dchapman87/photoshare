<?php

namespace Tests\Feature;

use Tests\ApiTestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Event;
use App\Events\AlbumCreated;
use App\Events\AlbumUpdated;
use App\Events\AlbumDeleted;
use App\Album;

class UserAlbumsTest extends ApiTestCase
{
    /**
     * Test the route exists.
     *
     * @return void
     */
    public function testRouteExists()
    {
        $response = $this->get(sprintf('api/v1/users/%s/albums', $this->user->id));
        $response->assertStatus(200);
    }

    /**
     * Test the json response structure using json schema.
     */
    public function test_json_structure_for_paginated_response()
    {
        $response = $this->get(sprintf('api/v1/users/%s/albums', $this->user->id));
        $response->assertStatus(200);
        $this->assertJsonSchema(json_decode($response->content()), 'Albums.json');
    }

    /**
     * Test creating a new album
     */
    public function test_can_create_new_user_album()
    {
        Storage::fake('logos');
        $album = [
            'title' => 'Test Album',
            'description' => 'Album for testing',
            'tags' => [
                'tag1',
                'tag2',
                'tag3'
            ],
            'logo' => UploadedFile::fake()->image('album.png')
        ];
        $before = $this->user->albums->count();
        $response = $this->postJson(
            sprintf('api/v1/users/%s/albums', $this->user->id),
            $album
        );
        $response->assertStatus(201);
        $created = json_decode($response->content());
        $this->assertEquals($created->title, $album['title']);
        $this->assertEquals(3, collect($created->tags)->count());
        $after = $this->user->fresh()->albums->count();
        $this->assertTrue($before < $after);
        $this->assertEquals($before + 1, $after);
        Storage::assertExists($created->logo);
    }

    public function test_can_update_user_album()
    {
        $album = Album::first();
        $album->title = "New Title";
        $response = $this->patchJson(
            sprintf('api/v1/users/%s/albums/%s', $this->user->id, $album->id),
            $album->toArray()
        );
        $response->assertStatus(200);
        $updated = json_decode($response->content());
        $this->assertEquals($updated->_id, $album->id);
        $this->assertEquals($updated->title, $album->title);
    }

    public function test_delete_user_album()
    {
        $album = Album::first();
        $albumCount = Album::all()->count();
        $response = $this->delete(
            sprintf('api/v1/users/%s/albums/%s', $this->user->id, $album->id)
        );
        $response->assertStatus(204);
        $this->assertEquals(Album::all()->count(), $albumCount - 1);
    }
}