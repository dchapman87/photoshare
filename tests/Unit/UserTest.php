<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use App\User;

class UserTest extends TestCase
{
    private $user;

    public function setUp() : void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    public function tearDown() : void
    {
        DB::collection('users')->delete();
    }
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testUser()
    {
        $this->assertNotNull($this->user->name);
        $this->assertNotNull($this->user->email);
        $this->assertNotNull($this->user->username);
        $this->assertNotNull($this->user->password);
    }

    // public function test_user_appends_albums_url()
    // {
        
    // }
}
